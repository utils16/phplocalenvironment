DIR_PROJECT=# enter your dir_project here. Example: ~/home/user/project
DIR_GIT=# enter your dir_project here. Example: git@gitlab.com:project.git
DIR_ENV_PROJECT_FILES=projectEnvFiles/ # folder with project envs files
DIR_ENV_LOCAL=$(DIR_ENV_PROJECT_FILES).env.local
DIR_ENV_TEST=$(DIR_ENV_PROJECT_FILES)/.env.test.local

error:
	@echo "Please choose a target (install, run, stop, unit-tests...) or type 'make help' for more info"

install:
	@if [ -d $(DIR_PROJECT) ]; then rm -Rf $(DIR_PROJECT); fi
	@mkdir -p $(DIR_PROJECT)
	@git clone $(DIR_GIT) $(DIR_PROJECT)
	@chmod -R 777 $(DIR_ENV_PROJECT_FILES)
	@cp $(DIR_ENV_LOCAL) $(DIR_PROJECT) && cp $(DIR_ENV_TEST) $(DIR_PROJECT)
	@if grep "[enter here your Homestead.yaml configured site]" /etc/hosts; then echo '[enter here your Homestead.yaml configured site] exists inside /etc/hosts'; else echo '192.168.10.10 [enter here your Homestead.yaml configured site]' | sudo tee -a /etc/hosts; fi
	@vagrant destroy -f
	@vagrant up --provision
	@vagrant ssh --command "cd $(DIR_PROJECT) && composer install"

run:
	@vagrant up

run-provision:
	@vagrant up --provision

stop:
	@vagrant halt

phpunit:
	@vagrant ssh --command "cd $(DIR_PROJECT) && vendor/bin/phpunit"

phpunit-coverage:
	@vagrant ssh --command "cd $(DIR_PROJECT) && vendor/bin/phpunit --coverage-html=public/coverage"

composer-install:
	@vagrant ssh --command "cd $(DIR_PROJECT) && composer install"

composer-update:
	@vagrant ssh --command "cd $(DIR_PROJECT) && composer update"

ssh:
	@vagrant ssh

help:
	@echo "make install             - installs the local environment"
	@echo "make run                 - starts the local environment"
	@echo "make run-provision       - starts the local environment with --provision"
	@echo "make stop                - stops the local environment"
	@echo "make phpunit             - runs the phpunit test"
	@echo "make phpunit-coverage    - runs the phpunit test with coverage"
	@echo "make composer-install    - runs composer install into project"
	@echo "make composer-update     - runs composer update into project"
	@echo "make ssh                 - enter into virtual machine"

.PHONY: error install run run-provision stop phpunit composer-install composer-update