#### ENABLE XDEBUG
sudo phpenmod xdebug

if grep -Fxq "xdebug.remote_autostart = 1" /etc/php/7.4/cli/conf.d/20-xdebug.ini
	then
	    echo 'php-xdebug is already activated'
	else
	    echo 'xdebug.remote_autostart = 1' | sudo tee -a /etc/php/7.4/cli/conf.d/20-xdebug.ini
	    sudo /etc/init.d/php7.4-fpm restart
fi