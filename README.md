## Introduction

The main objective of the project is the installation and execution for our PHP code projects.

## Requirements

* Have Vagrant version 2.2.9 installed to avoid checksum problems
* Have Virtualbox installed (the project has been tested in VirtualBox 5).

## Installation

Launch the following command to clone the local environment project in the directory we want:
`git clone git@gitlab.com:utils/phpLocalEnvironment.git`

Once cloned, enter the cloned project directory and customize the Homestead.yaml file and Makefile files with your project properties. IMPORTANT!!!

Then, launch the `make install` command. It will take a few minutes as you have to download the Vagrant image

When it is finished, we will have our local environment ready to work. To stop this environment, we can launch the command `make stop` and the virtualization will stop.

## Makefile

Inside the project, there is a Makefile to simplify the execution of some commands:

###### help

Launches help about avaiables make commands

###### install

The main command. It allows install the project. Destroys the project if exists and runs a localEnvironment based on Homestead.

###### run

Starts vagrant virtual machine and local environment

###### run-provision

Starts vagrant virtual machine with --provision

###### stop

Stops vagrant virtual machine and local environment

###### phpunit

Runs unit test into the project

###### phpunit-coverage

Runs coverage command and generates a report about project coverage

###### composer-install

Runs composer install into the project

###### composer-update

Runs composer update into the project

###### ssh

Get inside the Vagrant virtual machine



